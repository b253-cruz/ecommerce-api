const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


router.post("/", auth.verify, (req,res) => {

		const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.addProduct(req.body)
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	}else{
		res.send(false);
	}

});



router.get("/allProducts", (req,res) => {

		const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.getAllProducts()
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	}else{
		res.send(false);
	}
});


router.get("/allActive", (req,res) =>{
		productController.getAllActive()
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));

})

router.get("/:id", (req, res) => {
	productController.getProduct(req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err));
});

router.put("/:id", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.updateProduct(req.params.id, req.body)
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	}else{
		res.send(false);
	}
});

router.patch("/:productId/archive/active", auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.archiveProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	}else{
		res.send(false);
	}

})





module.exports = router;
