const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body)
	.then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err))
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err))
});


router.post("/login", (req,res) => {
	userController.loginUser(req.body)
	.then(resultFromController => res.send( resultFromController))
	.catch(err => res.send(err));
});


router.post("/checkout", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId : userData.id,
        isAdmin : userData.isAdmin,
    }
    console.log(data)

    if(!data.isAdmin){

        userController.checkout(data, req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(err =>res.send(err));
    } else {
        res.send(false);
    }
});

router.get("/:userId/userDetails", (req,res) => {

	
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	userController.getProfile(req.params.userId)
	.then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err));
	}else{
		res.send(false);
	}

});


module.exports = router;
