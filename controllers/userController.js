const User = require("../models/Users");
const auth = require("../auth");
const Product = require("../models/Products");
const bcrypt = require("bcrypt");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else {
			return false;
		}
	}).catch(err => err);
};


module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(user => {
		if(user){
			return true
		}else {
			return false
		}
	}).catch(err => err)
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result === null){
				return false;
		}else{			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){			
				return  { access : auth.createAccessToken(result)}
			}else{
				return false;
				}
		}
	}).catch(err => err);
};


module.exports.checkout = async (data, body) => {
  const { userId } = data;

  let totalPrice = 0;

  let isUserUpdated = await User.findById(userId).then((user) => {
    user.orderedProducts.push({ products: body });

    return user
	      .save()
	      .then((user) => true)
	      .catch((err) => false);
  });

  let isProductUpdated = await Product.findById(body.productId)
  .then((product) => {

  	const {price, quantity} = item;
  	const productTotalPrice = price*quantity;

  	totalAmount += productTotalPrice;

  	product.userOrders.push({userId,quantity})

  	return product
		  .save()
		  .then((product) =>true)
		  .catch((err) => false);
  });

  console.log(isUserUpdated)
  console.log(isProductUpdated)

  if(isUserUpdated && isProductUpdated){
  	return true;
  }else{
  	return false
  };
};


module.exports.getProfile = (data) => {
	return User.findById(data.userId)
	.then(result => {

		console.log(user,id)
		user.password = "";
		return result;
	}) .catch(err => err);
};







