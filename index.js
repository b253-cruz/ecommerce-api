const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://admin:admin123@b253-cruz.hwbaeqz.mongodb.net/capstone?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

const db = mongoose.connection

db.once("open", () => console.log("Now connected to MongoDB Atlas"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoute);
app.use("/products", productRoute);


if(require.main === module){
	app.listen(port, () =>{
		console.log(`API is now online on port ${port}`)
	});
}
module.exports = app;